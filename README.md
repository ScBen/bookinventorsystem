BookInventorySystem API

Download the application

Stack used:
  1.Django
  2.Postgresql database

Git: Gitlab

Prerequisites:
Make sure you install the requirements before you run the project:
   pip install -r requirements.txt

Navigate to the BookInventorySystem folder and run the following command:
   $ python manage.py runserver

Adjust the settings in settings.py file according to your set up.