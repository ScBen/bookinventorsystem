from rest_framework import serializers
from .models import Author, Book, Stock

#Serializing authors
class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = 'first_name', 'last_name', 'email', 'date_of_birth'

#Serializing books
class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = 'title', 'author', 'year_of_publication', 'description', 'stock'
        
#Serializing and tracking stock status
class StockSerializer(serializers.ModelSerializer):
    quantity = serializers.IntegerField(required=False, default=1)
    status = serializers.SerializerMethodField('get_status')
    class Meta:
        model = Stock
        fields = 'quantity', 'status'
    def get_status(self, instance):
        status_set = instance.quantity
        
        if status_set >= 10:
            status_set = 'Good'
        elif status_set in range(5,10):
            status_set = 'Bad'
        elif status_set in range(1,5):
            status_set = 'Critical'
        elif status_set == 0:
            status_set = 'Out_of_stock'
                  
        return status_set