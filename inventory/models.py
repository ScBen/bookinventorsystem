from django.db import models
from django.contrib.postgres.fields import IntegerRangeField
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator

#get the current year to restrict upper limit of the year of publication
def current_year():
    return datetime.date.today().year

def max_value_for_year(value):
    return MaxValueValidator(current_year())(value)


# Create Author model(database table implementation)
class Author(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=254)
    date_of_birth = models.DateField()

# Create Stock model(database table implementation)
class Stock(models.Model):
    quantity = models.PositiveIntegerField()

# Create Book model(database table implementation)    
class Book(models.Model):
    title = models.CharField(max_length=50)
    author = models.ForeignKey(Author, on_delete=models.PROTECT)
    year_of_publication = models.IntegerField(('year'), validators=[MinValueValidator(1000),max_value_for_year])
    description = models.CharField(max_length=250)
    stock = models.ForeignKey(Stock, on_delete=models.PROTECT) 


