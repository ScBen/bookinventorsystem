from django.urls import path, include
from . import views
from .views import BookViews

app_name='inventory'

#url patterns for inventory api
urlpatterns = [
    path('find_books/', views.find_books, name='find_books_year'),
    path('book_stock/', BookViews.as_view()),
    path('book_stock/<int:id>', BookViews.as_view())
]