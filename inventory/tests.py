from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.db import models

from inventory.models import Author, Stock, Book

#Testing creation of each model
class BookTestCase(TestCase):

    def setUp(self):
        author= Author.objects.create(first_name='Benedict',last_name='Musila',email='musilabeny@gmail.com',date_of_birth='1990-10-02')
        stock = Stock.objects.create(quantity='6')
        self.book_title= "Create a perfect book model"
        self.book_author= author
        self.book_year_of_publication= 1997
        self.book_description= 'Success in life'
        self.book_stock = stock
        self.book = Book(title=self.book_title,author=self.book_author,year_of_publication=self.book_year_of_publication,stock=self.book_stock)
        self.author_first_name = 'Paul'
        self.author_last_name = 'Coelho'
        self.author_email = 'paulalchemist@yahoo.com'
        self.author_date_of_birth = '1987-09-09'
        self.author= Author(first_name=self.author_first_name,last_name=self.author_last_name,email=self.author_email,date_of_birth=self.author_date_of_birth)
        self.stock_quantity = 100
        self.stock = Stock(quantity=self.stock_quantity)

    def test_model_can_create_a_book(self):
        first_count = Book.objects.count()
        self.book.save()
        new_count = Book.objects.count()
        self.assertNotEqual(first_count, new_count)

    def test_model_can_create_author(self):
        first_count_author = Author.objects.count()
        self.author.save()
        new_count_author = Author.objects.count()
        self.assertNotEqual(first_count_author, new_count_author)

    def test_model_can_create_stock(self):
        first_count_stock = Stock.objects.count()
        self.stock.save()
        new_count_stock = Stock.objects.count()
        self.assertNotEqual(first_count_stock, new_count_stock)

#Testing api calls 
class BookApiTests(APITestCase):
    def setUp(self):
        self.data = {
            "title": "Lost in space",
            "author": "Bennito Ndunda",
            "year_of_publication": 2008,
            "description" : "Adventure and travel",
            "stock" : 20,
        }
        self.response = self.client.post(
            reverse('books'),
            self.data,
            format="json")

    def test_api_create_book(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Book.objects.count(), 1)
        self.assertEqual(Book.objects.get().title, 'Lost in space')

    def test_api_list_books(self):
        url = reverse('books')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Book.objects.count(), 1)