from django.shortcuts import render
from rest_framework import generics, viewsets
from .serializers import AuthorSerializer, BookSerializer, StockSerializer
from rest_framework.response import Response
from .models import Author, Book, Stock
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework import status
from inventory import serializers

#filter books by author and year of publication
@api_view(['GET', ])
def find_books(request):
    author = request.query_params.get('author', None)
    year_of_publication = request.query_params.get('year_of_publication', None)
    queryset = Book.objects.all()
    if author:
        queryset = queryset.filter(author__first_name__contains=author)
    if author:
        queryset = queryset.filter(author__last_name__contains=author)
    if year_of_publication:
        queryset = queryset.filter(year_of_publication__contains=year_of_publication)
    serializer = BookSerializer(queryset, many=True)
    return Response(serializer.data)

#list ,get and post books
class BookViewset(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
#list, get and post authors
class AuthorViewset(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
#list, get and post stock quantities
class StockViewset(viewsets.ModelViewSet):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer

#get, update and post all book information including stock 
class BookViews(APIView):
    def post(self, request):
        serializer = BookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)
        else:
            return  Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, id=None):
        if id:
            book = Book.objects.get(id=id)
            serializer = BookSerializer(book)
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)

        books = Book.objects.all()
        serializer = BookSerializer(books, many=True)
        return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK) 

    def patch(self, request, id=None):
        book = Book.objects.get(id=id)
        serializer = BookSerializer(book, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": "success", "data": serializer.data})
        else:
            return Response({"status": "error", "data": serializer.errors})   